import json
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--files", type=str, default="/data/jiangxue/pope/answers/answer-pope-nstep999.jsonl")

args = parser.parse_args()

# 读取jsonl文件
file = args.files
file_name = os.path.basename(file)
file_path = os.path.dirname(file)
filename_without_extension = os.path.splitext(file_name)[0]

# 读取JSONL文件并解析为字典列表
data = []
with open(os.path.expanduser(args.files), "r") as f:
    for line in f:
        data.append(json.loads(line))

# 将数据按照id的范围特性分成三个列表
group1 = [item for item in data if 1 <= item["question_id"] <= 3000]
group2 = [item for item in data if 10000001 <= item["question_id"] <= 10003000]
group3 = [item for item in data if 20000001 <= item["question_id"] <= 20003000]

# 写入三个分组到新的JSONL文件
with open(os.path.join(file_path, filename_without_extension + '-adversarial.jsonl'), "w") as f:
    for item in group1:
        json.dump(item, f)
        f.write('\n')

with open(os.path.join(file_path, filename_without_extension + '-random.jsonl'), "w") as f:
    for item in group2:
        json.dump(item, f)
        f.write('\n')

with open(os.path.join(file_path, filename_without_extension + '-popular.jsonl'), "w") as f:
    for item in group3:
        json.dump(item, f)
        f.write('\n')

print("Done!")
