from imgaug import augmenters as iaa
import numpy as np
import cv2
import os
from PIL import Image


def img_aug(img, selected_augmentation):
    # 读取图像
    # image = cv2.imread(img)

    if selected_augmentation == 'multiply':
        aug = iaa.Sequential([iaa.Multiply((0.8, 1.2))])
        aug_name = "Multiply"
    elif selected_augmentation == 'contrast':
        aug = iaa.Sequential([iaa.contrast.LinearContrast((0.75, 1.5))])
        aug_name = "Contrast"
    elif selected_augmentation == 'sharpen':
        aug = iaa.Sequential([iaa.Sharpen(alpha=(0, 1.0), lightness=(0.75, 1.5))])
        aug_name = "Sharpen"
    elif selected_augmentation == 'blur':
        aug = iaa.Sequential([iaa.GaussianBlur(sigma=(0, 3.0))])
        aug_name = "Blur"
    elif selected_augmentation == 'noise':
        aug = iaa.Sequential([iaa.AdditiveGaussianNoise(scale=(0, 0.05*255))])
        aug_name = "Noise"
    elif selected_augmentation == 'colorspace':
        aug = iaa.Sequential([iaa.WithColorspace(to_colorspace="HSV", from_colorspace="RGB",
                        children=iaa.WithChannels(0, iaa.Add((10, 50))))])
        aug_name = "Colorspace"
    elif selected_augmentation == 'translate':
        aug = iaa.Sequential([iaa.Affine(translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)})])
        aug_name = "Translate"
    elif selected_augmentation == 'rotate':
        aug = iaa.Sequential([iaa.Affine(rotate=(-45, 45))])
        aug_name = "Rotate"
    elif selected_augmentation == 'flip_lr':
        aug = iaa.Sequential([iaa.Fliplr(1.0)])
        aug_name = "Flip Left-Right"
    elif selected_augmentation == 'flip_ud':
        aug = iaa.Sequential([iaa.Flipud(1.0)])
        aug_name = "Flip Up-Down"
    elif selected_augmentation == 'scale':
        aug = iaa.Sequential([iaa.Affine(scale={"x": (0.8, 1.2), "y": (0.8, 1.2)})])
        aug_name = "Scale"
    elif selected_augmentation == 'brightness':
        aug = iaa.Sequential([iaa.Multiply((0.5, 1.5))])
        aug_name = "Brightness Adjustment"
    elif selected_augmentation == 'contrast_norm':
        aug = iaa.Sequential([iaa.ContrastNormalization((0.5, 1.5))])
        aug_name = "Contrast Normalization"
    elif selected_augmentation == 'motion_blur':
        aug = iaa.Sequential([iaa.MotionBlur(k=(3, 7))])
        aug_name = "Motion Blur"
    elif selected_augmentation == 'emboss':
        aug = iaa.Sequential([iaa.Emboss(alpha=(0, 1.0), strength=(0, 2.0))])
        aug_name = "Emboss"
    elif selected_augmentation == 'elastic':
        aug = iaa.Sequential([iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)])
        aug_name = "Elastic Transformation"
    elif selected_augmentation == 'crop':
        aug = iaa.Sequential([iaa.Crop(percent=(0.1, 0.3))])
        aug_name = "Crop"
    elif selected_augmentation == 'pad':
        aug = iaa.Sequential([iaa.Pad(percent=(0, 0.2))])
        aug_name = "Pad"
    elif selected_augmentation == 'shear':
        aug = iaa.Sequential([iaa.Affine(shear=(-16, 16))])
        aug_name = "Shear"
    elif selected_augmentation == 'sigmoid_contrast':
        aug = iaa.Sequential([iaa.SigmoidContrast(gain=(3, 10), cutoff=(0.4, 0.6))])
        aug_name = "Sigmoid Contrast"
    elif selected_augmentation == 'piecewise_affine':
        aug = iaa.Sequential([iaa.PiecewiseAffine(scale=(0.01, 0.05))])
        aug_name = "Piecewise Affine"
    elif selected_augmentation == 'affine':
        aug = iaa.Sequential([iaa.Affine(scale=(0.5, 1.5))])
        aug_name = "Affine"

    image_np = np.array(img)

    # 应用增强
    image_aug = aug(image=image_np)
    image_aug = Image.fromarray(image_aug)

    # 应用增强
    # image_aug = aug(image=image)
    return image_aug, aug_name



if __name__ == "__main__":
    all_augmentations = [
    'multiply', 'contrast', 'sharpen', 'blur', 'noise', 'colorspace', 
    'translate', 'rotate', 'flip_lr', 'flip_ud', 'scale', 'brightness', 
    'contrast_norm', 'motion_blur', 'emboss', 'elastic', 'crop', 'pad', 
    'shear', 'sigmoid_contrast', 'piecewise_affine', 'affine']

    # 创建保存增强后图片的文件夹
    output_folder = "/ssd2/jiangxue/augmented_images"
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # 读取示例图像
    img_path = "/ssd2/jiangxue/MME/MME_Benchmark_release_version/scene/images/Places365_val_00000107.jpg"
    image = cv2.imread(img_path)

    # 遍历所有增强方法并进行增强
    for augmentation in all_augmentations:
        # 执行增强
        augmented_image, aug_name = img_aug(img_path, augmentation)
        
        # 构建输出路径
        output_path = os.path.join(output_folder, f"{aug_name}.jpg")

        # 保存增强后的图像
        cv2.imwrite(output_path, augmented_image)



