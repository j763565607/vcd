# hyper for POPE in COCO
# python ./experiments/eval/object_hallucination_vqa_llava.py \
#     --model-path liuhaotian/llava-v1.5-7b \
#     --question-file /data/jiangxue/pope/llava_pope_test.jsonl \
#     --image-folder /data/jiangxue/pope/val2014 \
#     --answers-file /data/jiangxue/pope/answers/answer-pope-nstep999-temper0.001-blur.jsonl \
#     --conv-mode vicuna_v1 \
#     --temperature 0.001 \
#     --use_cd \
#     --noise_step 999


# python ./experiments/eval/object_hallucination_vqa_llava.py \
#     --model-path liuhaotian/llava-v1.5-7b \
#     --question-file /data/jiangxue/pope/llava_pope_test.jsonl \
#     --image-folder /data/jiangxue/pope/val2014 \
#     --answers-file /data/jiangxue/pope/answers/answer-pope-nocd.jsonl \
#     --conv-mode vicuna_v1 \
#     --temperature 0.001 \
    # --use_cd \ 
    # --noise_step 999

# MME
    python ./experiments/eval/object_hallucination_vqa_llava.py \
    --model-path liuhaotian/llava-v1.5-7b \
    --question-file  /ssd2/jiangxue/MME/llava_mme.jsonl \
    --image-folder /ssd2/jiangxue/MME/MME_Benchmark_release_version \
    --answers-file /ssd2/jiangxue/MME/answers/llava15-mme-nstep500-temper1.jsonl \
    --conv-mode vicuna_v1 \
    --temperature 0.7 \
    --use_cd \
    --noise_step 500